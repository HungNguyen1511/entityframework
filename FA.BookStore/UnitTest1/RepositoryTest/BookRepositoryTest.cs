﻿using FA.BookStore.Core.Data;
using FA.BookStore.Core.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Service.InterFace;
using Service.RepositoryEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assert = NUnit.Framework.Assert;

namespace UnitTest1.RepositoryTest
{
    [TestClass]
    public class BookRepositoryTest
    {
        IBookRepository objRepository;
        BookStoreContext db = new BookStoreContext();
        [TestInitialize]
        public void Initialie()
        {
            objRepository = new BookRepository(db);
        }
        [TestMethod]
        public void BookRepository_GetAll()
        {
            var list = objRepository.GetAll().ToList();
            Assert.AreEqual(4, list.Count);
        }

        [TestMethod]
        public void BookRepository_Find()
        {
            Book book = new Book() { BookId = 1,Title = "De men phieu luu ky", 
                CateId = 1, PubId = 1, Price = 1200, ImgUrl = "C//folder", Quantity = 5, Summary = "Truyen nay hay" };
            var result = objRepository.Find(1);
            Assert.AreEqual(book.BookId, result.BookId);
        }

        [TestMethod]
        public void BookRepository_Add()
        {
            Book book = new Book();
            book.Title = "Title-Book";
            book.CateId = 1;
            book.Price = 1000;
            book.PubId = 1;
            book.Quantity = 1;
            book.Summary = "Sach nay danh cho moi nguoi";
            book.ImgUrl = "C/folder";
            var result = objRepository.Add(book);
            objRepository.Commit();
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.BookId);
        }

        [TestMethod]
        public void BookRepository_Delete()
        {
            Book book = objRepository.Find(1);
            int count = objRepository.GetAll().Count();
            objRepository.Delete(book);
            objRepository.Commit();
            Assert.AreEqual(count - 1, objRepository.GetAll().Count());
        }

        [TestMethod]
        public void BookRepository_Update()
        {
            string after = "Mot cai gi day";
            Book book = objRepository.Find(2);
            book.Title = after;
            objRepository.Update(book);
            Assert.AreEqual(after, objRepository.Find(2).Title);
        }
    }
}
