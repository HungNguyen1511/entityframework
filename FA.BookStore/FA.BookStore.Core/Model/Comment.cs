﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.BookStore.Core.Model
{
    
    public class Comment
    {
        [Key]
        public int CommentId { set; get; }
        public string _Comment { set; get; }
        public DateTime CreatedDate { set; get; }
        public bool IsActive { set; get; }
        public int BookId { set; get; }
        //public virtual Book Book { set; get; }
    }
}
