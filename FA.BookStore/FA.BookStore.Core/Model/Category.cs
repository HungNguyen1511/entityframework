﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.BookStore.Core.Model
{

    public class Category
    {
        [Key]
        public int CateId { set; get; }
        [Required(ErrorMessage = "Category name can be not empty")]
        public string CateName { set; get; }
        public string Description { set; get; }
        public virtual ICollection<Book> Books { set; get; }

    }
}
