﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.BookStore.Core.Model
{
    public class Book
    {
        [Key]
        public int BookId { get; set; }
        [MaxLength(50)]
        public string Title { get; set; }
        [ForeignKey("Category")]
        public int CateId { set; get; }
        public virtual Category Category { set; get; }
        public int AuthorId { set; get; }
        [ForeignKey("Publisher")]
        public int PubId { set; get; }
        public virtual Publisher Publisher { set; get; }
        public string Summary { set; get; }
        public string ImgUrl { set; get; }
        [Range(1000, 9999)]
        public decimal Price { set; get; }
        public int Quantity { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public DateTime? CreateDate { set; get; }
        public DateTime? ModifiedDate { set; get; }
        public bool? IsActive { set; get; }

    }
}
