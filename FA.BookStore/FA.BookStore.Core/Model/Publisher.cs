﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.BookStore.Core.Model
{
  
    public class Publisher
    {
        [Key]
        public int PubId { set; get; }
        [Required(ErrorMessage = "Publisher name can be not empty")]
        public string Name { set; get; }
        public string Description { set; get; }
        public virtual ICollection<Book> Books { set; get; }
    }
}
