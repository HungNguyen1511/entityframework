﻿using FA.BookStore.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.BookStore.Core.Data
{
    public class BookStoreInitializer : DropCreateDatabaseAlways<BookStoreContext>
    {
        protected override void Seed(BookStoreContext context)
        {
            base.Seed(context);

            //// Tạo sẵn một vài dữ liệu
            //// Master data: dữ liệu cấu hình, dữ liệu có sẵn
           
            context.Categories.Add(new Category() { CateId =1,CateName = "Truyen Thieu Nhi",Description ="Sap Xuat ban" });
            context.Categories.Add(new Category() { CateId =2,CateName = "Van Hoc Viet Nam", Description = "Chuan Bi Xuan Ban" });
            context.Categories.Add(new Category() { CateId =3,CateName = "Van Hoc Nuoc Ngoai", Description = "Tai Ban" });

            context.Publishers.Add(new Publisher() { PubId =1, Name = "Nha Xuat Ban Tre" ,Description = "Tru so Duong Lang"});
            context.Publishers.Add(new Publisher() { PubId = 2, Name = "Nha Xuat Ban Giao Duc",Description = "Dung Hoat dong do covid" });
            context.Publishers.Add(new Publisher() { PubId = 3, Name = "Nha Xuat Ban Thoi Dai",Description ="Dang hoat dong" });

            context.Books.Add(new Book() { Title = "De men phieu luu ky", CateId = 1, PubId = 1, Price = 1200, ImgUrl = "C//folder", Quantity = 5, Summary = "Truyen nay hay" });
            context.Books.Add(new Book() { Title = "Luoc Su Thoi Gian", CateId = 2, PubId = 2, Price = 1600, ImgUrl = "C//folder", Quantity = 4, Summary = "Truyen nay tam duoc" });
            context.Books.Add(new Book() { Title = "Truyen Co Grim", CateId = 2, PubId = 3, Price = 1800, ImgUrl = "C//folder", Quantity = 3, Summary = "Truyen nay binh thuong" });
            context.SaveChanges();
            // đạt bảo chỉ cần save change ở cuối là xong - Master data 
            //// Tạo thêm dữ liệu cho Comment, ....
        }
    }

    public class BookStoreCreateIfExits_Initializer : CreateDatabaseIfNotExists<BookStoreContext>
    {
        protected override void Seed(BookStoreContext context)
        {
            base.Seed(context);

            //// Tạo sẵn một vài dữ liệu
            //// Master data: dữ liệu cấu hình, dữ liệu có sẵn

            context.Categories.Add(new Category() { CateId = 1, CateName = "Truyen Thieu Nhi", Description = "Sap Xuat ban" });
            context.Categories.Add(new Category() { CateId = 2, CateName = "Van Hoc Viet Nam", Description = "Chuan Bi Xuan Ban" });
            context.Categories.Add(new Category() { CateId = 3, CateName = "Van Hoc Nuoc Ngoai", Description = "Tai Ban" });

            context.Publishers.Add(new Publisher() { PubId = 1, Name = "Nha Xuat Ban Tre", Description = "Tru so Duong Lang" });
            context.Publishers.Add(new Publisher() { PubId = 2, Name = "Nha Xuat Ban Giao Duc", Description = "Dung Hoat dong do covid" });
            context.Publishers.Add(new Publisher() { PubId = 3, Name = "Nha Xuat Ban Thoi Dai", Description = "Dang hoat dong" });

            context.Books.Add(new Book() { Title = "De men phieu luu ky", CateId = 1, PubId = 1, Price = 1200, ImgUrl = "C//folder", Quantity = 5, Summary = "Truyen nay hay" });
            context.Books.Add(new Book() { Title = "Luoc Su Thoi Gian", CateId = 2, PubId = 2, Price = 1600, ImgUrl = "C//folder", Quantity = 4, Summary = "Truyen nay tam duoc" });
            context.Books.Add(new Book() { Title = "Truyen Co Grim", CateId = 2, PubId = 3, Price = 1800, ImgUrl = "C//folder", Quantity = 3, Summary = "Truyen nay binh thuong" });
            context.SaveChanges();
            // đạt bảo chỉ cần save change ở cuối là xong - Master data 
            //// Tạo thêm dữ liệu cho Comment, ....
        }
    }
}
