﻿using FA.BookStore.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.BookStore.Core.Data
{
    public class BookStoreContext :DbContext
    {
        public BookStoreContext() : base("AssigmentEntityFramework")
        { 
            //// Lấy tên class context làm tên của connection string
            Database.SetInitializer<BookStoreContext>(new BookStoreCreateIfExits_Initializer());
            ///
            //Database.SetInitializer<BookStoreContext>(new BookStoreInitializer());

            //Database.SetInitializer<HrmContext>(new DropCreateDatabaseAlways<HrmContext>());
            //Database.SetInitializer<HrmContext>(new MigrateDatabaseToLatestVersion<HrmContext, Migrations.Configuration>());        
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Student>().HasMany<Book>(st => st.ReadBooks).WithMany(b => b.Students).Map(r =>
            //{
            //    r.MapLeftKey("StudentCode");
            //    r.MapRightKey("ISBN");
            //    r.ToTable("ReadingRelation");
            //});
            //// Dẫn đến thay đổi cấu trúc dữ liệu => mapping bị thay đổi theo => mapping lại
            //// DB first: Update model from Database -> DONE
            //// Code first
        }
        public DbSet<Book> Books { set; get; }
        public DbSet<Category> Categories { set; get; }
        public DbSet<Publisher> Publishers { set; get; }
        public DbSet<Comment> Comments { set; get; }
    }
}
