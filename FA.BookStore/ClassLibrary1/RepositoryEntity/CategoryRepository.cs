﻿using FA.BookStore.Core.Data;
using FA.BookStore.Core.Model;
using Service.InterFace;
using Service.RepsitoryBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.RepositoryEntity
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository(BookStoreContext db) : base(db)
        {

        }
    }
}
