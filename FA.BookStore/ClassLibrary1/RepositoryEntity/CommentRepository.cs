﻿using FA.BookStore.Core.Data;
using FA.BookStore.Core.Model;
using Service.InterFace;
using Service.RepsitoryBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.RepositoryEntity
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        private readonly BookStoreContext _context;
        public CommentRepository(BookStoreContext context) : base(context)
        {
            _context = context;
        }
        public IEnumerable<Comment> GetCommentByBook(int bookId)
        {
            return _context.Comments.Where(x => x.BookId == bookId).AsEnumerable();
        }
    }
}
