﻿using FA.BookStore.Core.Data;
using FA.BookStore.Core.Model;
using Service.InterFace;
using Service.RepsitoryBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.RepositoryEntity
{
    public class BookRepository : RepositoryBase<Book>, IBookRepository
    {
        private readonly BookStoreContext _context;

        public BookRepository(BookStoreContext context) : base(context)
        {
            _context = context;
        }

        public int CountBooksForCategory(string categoryName)
        {
            var list = _context.Books.Where(x => x.Category.CateName.Contains(categoryName)).AsEnumerable();
            return list.Count();
        }

        public int CountBooksForPublisher(string publisherName)
        {
            var list = _context.Books.Where(x => x.Publisher.Name.Contains(publisherName)).AsEnumerable();
            return list.Count();
        }

        public IEnumerable<Book> FindBookBySummary(string summary)
        {
            return _context.Books.Where(x => x.Summary.Contains(summary)).AsEnumerable();
        }

        public IEnumerable<Book> FindBookByTitle(string title)
        {
            return _context.Books.Where(x => x.Summary.Contains(title)).AsEnumerable();
        }

        public IEnumerable<Book> GetBooksByCategory(string categoryName)
        {
            return _context.Books.Where(x => x.Category.CateName.Contains(categoryName)).AsEnumerable();
        }

        public IEnumerable<Book> GetBooksByMonth(DateTime monthYear)
        {
            return _context.Books.Where(x => x.ModifiedDate == monthYear);
        }
        public IEnumerable<Book> GetBooksByPublisher(string publisherName)
        {
            return _context.Books.Where(x => x.Publisher.Name.Contains(publisherName)).AsEnumerable();
        }
        public IEnumerable<Book> GetLatestBook(int count)
        {
            var listBook = _context.Books.OrderByDescending(x=> x.CreateDate).Take(count).AsEnumerable();
            return listBook;
        }
    }
}
