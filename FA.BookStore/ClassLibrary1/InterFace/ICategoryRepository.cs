﻿using FA.BookStore.Core.Model;
using Service.RepsitoryBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.InterFace
{
    public interface ICategoryRepository : IRepositoryBase<Category>
    {
    }
}
