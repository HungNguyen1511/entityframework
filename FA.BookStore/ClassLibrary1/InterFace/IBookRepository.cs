﻿using FA.BookStore.Core.Model;
using Service.RepsitoryBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.InterFace
{
    public interface IBookRepository : IRepositoryBase<Book>
    {
        IEnumerable<Book> FindBookByTitle(string title);
        IEnumerable<Book> FindBookBySummary(string summary);
        IEnumerable<Book> GetLatestBook(int count);
        IEnumerable<Book> GetBooksByMonth(DateTime monthYear);
        int CountBooksForCategory(string categoryName);
        IEnumerable<Book> GetBooksByCategory(string categoryName);
        int CountBooksForPublisher(string publisherName);
        IEnumerable<Book> GetBooksByPublisher(string publisherName);
    }
}
