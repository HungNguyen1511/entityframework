﻿using FA.BookStore.Core.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.RepsitoryBase
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly BookStoreContext _db;
        internal DbSet<T> dbSet;

        public RepositoryBase(BookStoreContext db)
        {
            _db = db;
            this.dbSet = _db.Set<T>();
        }
        public virtual T Add(T entity)
        {
            return dbSet.Add(entity);
        }
        public virtual T Delete(T entity)
        {
            return dbSet.Remove(entity);
        }

        public virtual T Find(int id)
        {
            return dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.AsEnumerable();
        }
        public virtual T Update(T entity)
        {
            return dbSet.Attach(entity);
            _db.Entry(entity).State = EntityState.Modified;
            
        }
        public virtual void Commit()
        {
            _db.SaveChanges();
        }
    }
}
