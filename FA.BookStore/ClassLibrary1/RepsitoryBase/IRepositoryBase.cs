﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.RepsitoryBase
{
    public interface IRepositoryBase<T>
    {
        T Find(int id);
        T Add(T entity);
        T Update(T entity);
        T Delete(T entity);
        IEnumerable<T> GetAll();
        void Commit();
    }
}
